import glob
import re

import cv2
import numpy as np
from torch.utils.data import Dataset


def rgb2gray(rgb):

    r, g, b = rgb[:, :, 0], rgb[:, :, 1], rgb[:, :, 2]
    gray = 0.299 * r + 0.5870 * g + 0.1140 * b
    rgb_grey = rgb.copy()
    # matrix array transform
    rgb_grey[:, :, 0] = gray
    rgb_grey[:, :, 1] = gray
    rgb_grey[:, :, 2] = gray
    return rgb_grey


def mask_filter(image, grey_threshold):
    img = image.copy()
    grey_img = rgb2gray(img)
    convert = np.zeros((img.shape[0], img.shape[1], 3))
    idxs = np.where(
        (grey_img[:, :, 0] > grey_threshold)
        & (grey_img[:, :, 1] > grey_threshold)
        & (grey_img[:, :, 2] > grey_threshold)
    )
    convert[idxs] = [255, 255, 255]
    return np.uint8(convert)


def maximizeContrast(imgGrayscale):

    height, width = imgGrayscale.shape

    imgTopHat = np.zeros((height, width, 1), np.uint8)
    imgBlackHat = np.zeros((height, width, 1), np.uint8)

    structuringElement = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))

    imgTopHat = cv2.morphologyEx(imgGrayscale, cv2.MORPH_TOPHAT, structuringElement)
    imgBlackHat = cv2.morphologyEx(imgGrayscale, cv2.MORPH_BLACKHAT, structuringElement)

    imgGrayscalePlusTopHat = cv2.add(imgGrayscale, imgTopHat)
    imgGrayscalePlusTopHatMinusBlackHat = cv2.subtract(
        imgGrayscalePlusTopHat, imgBlackHat
    )

    return imgGrayscalePlusTopHatMinusBlackHat


def delete_color(img):
    image_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    image_hsv[:, :, 1] = np.where(
        image_hsv[:, :, 1] >= 10, np.random.randint(10), image_hsv[:, :, 1]
    )

    image = cv2.cvtColor(image_hsv, cv2.COLOR_HSV2BGR)
    return image


def detect_white_annotation(img):
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    dif = maximizeContrast(img_gray)
    dif_rgb = cv2.cvtColor(dif, cv2.COLOR_GRAY2BGR)
    masked_img = mask_filter(dif_rgb, 254)
    dilation = cv2.dilate(masked_img, np.ones((3, 3), np.uint8), iterations=1)
    mask = cv2.cvtColor(dilation, cv2.COLOR_BGR2GRAY)
    return mask


def detect_cyan(img):
    image_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    lowers = np.uint8([85, 150, 20])
    uppers = np.uint8([95, 255, 255])
    mask = np.array(cv2.inRange(image_hsv, lowers, uppers))
    return mask


def detect_color(img):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    boundaries = [
        ([40, 215, 100], [50, 225, 120]),
        ([0, 15, 60], [0, 125, 255]),
        ([80, 0, 0], [255, 115, 0]),
        ([10, 10, 120], [90, 90, 170]),
    ]

    mask = np.zeros((img.shape[0], img.shape[1]))
    # loop over the boundaries

    for (lower, upper) in boundaries:

        # create NumPy arrays from the boundaries
        lowers = np.array(lower, dtype="uint8")

        uppers = np.array(upper, dtype="uint8")

        mask = mask + np.array(cv2.inRange(img, lowers, uppers))
    mask = np.uint8(mask)
    if mask.mean() > 0.09:
        return True
    else:
        return False


def detect_annotation(img):
    inpaint_mask = detect_white_annotation(img) + detect_color(img) + detect_cyan(img)
    inpaint_mask = maximizeContrast(inpaint_mask)
    blur = cv2.GaussianBlur(inpaint_mask, (5, 5), 0)
    ret3, th3 = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    return th3


def stringSplitByNumbers(x):
    r = re.compile("(\d+)")
    l = r.split(x)
    return [int(y) if y.isdigit() else y for y in l]


class bsds_dataset(Dataset):
    def __init__(self, ds_main, ds_energy, transform=None):
        self.dataset1 = ds_main
        self.dataset2 = ds_energy
        self.transform = transform

    def __getitem__(self, index):
        x1 = self.dataset1[index]
        x2 = self.dataset2[index]
        if self.transform:
            x1 = self.transform(x1)
            x2 = self.transform(x2)

        return x1, x2

    def __len__(self):
        return len(self.dataset1)


class LargeDataset(Dataset):
    def __init__(self, path_images, mask=False):
        self.path = path_images
        self.mask = mask

    def __getitem__(self, index):
        img = self.path[index]
        image = cv2.imread(img)
        if self.mask == False:
            image = (image - np.min(image)) / (np.max(image) - np.min(image))
            return image
        else:
            img_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) / 255
            img_gray = np.expand_dims(img_gray, axis=2)
            return img_gray

    def __len__(self):
        return len(self.path)


def get_dataset(path_images, mask=False):
    path = sorted(glob.glob(path_images + "/*.png"), key=stringSplitByNumbers)
    intial_ds = []
    count = 0
    if mask == False:
        for img in path:
            count += 1
            print(count, flush=True)
            image = cv2.imread(img)
            image = (image - np.min(image)) / (np.max(image) - np.min(image))
            intial_ds.append(image)
    else:
        for img in path:
            image = cv2.imread(img)
            img_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) / 255
            img_gray = np.expand_dims(img_gray, axis=2)
            intial_ds.append(img_gray)
    intial_ds = np.array(intial_ds)
    return intial_ds
