import os
import sys

import torch.optim as optim
import torchvision
from tensorboard.program import TensorBoard
from torch.autograd import Variable
from torch.utils.tensorboard import SummaryWriter

from lib.model import *
from lib.utils import *
from lib.utils_test import LargeDataset


def launch_tensorboard():

    write = SummaryWriter()

    tbDir = os.path.abspath(write.log_dir) + "/"

    print(f"SummaryWriter is writing into {tbDir}")

    tb = TensorBoard()
    tb.configure(logdir=tbDir)
    url = tb.launch()
    return write


def training(
    train_loader,
    validation_loader,
    path_to_save_weights,
    epochs=50,
    steps=0,
    print_every=100,
):

    model = UNet(3, 1)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model.to(device)
    criterion = nn.BCEWithLogitsLoss(reduction="mean")
    optimizer = optim.Adam(model.parameters(), lr=0.001)
    running_loss = 0
    train_losses, test_losses = [], []
    write = launch_tensorboard()

    for epoch in range(epochs):
        for inputs, image_masks in train_loader:
            steps += 1
            inputs, image_masks = (
                Variable(inputs).transpose_(1, 3),
                Variable(image_masks).transpose_(1, 3),
            )
            optimizer.zero_grad()
            dec = model(inputs)
            loss = criterion(dec.double(), image_masks.double())
            loss.backward()
            optimizer.step()
            running_loss += loss.item()

            if steps % print_every == 0:
                test_loss = 0
                accuracy = 0
                diceloss = 0
                model.eval()
                with torch.no_grad():
                    for inputs, image_masks in validation_loader:
                        inputs, image_masks = (
                            Variable(inputs).transpose_(1, 3),
                            Variable(image_masks).transpose_(1, 3),
                        )
                        dec = model(inputs)
                        batch_loss = criterion(dec.double(), image_masks.double())
                        test_loss += batch_loss.item()
                        pred = torch.sigmoid(dec) > 0.5
                        size_pred = float(
                            pred.size()[0]
                            * pred.size()[1]
                            * pred.size()[2]
                            * pred.size()[3]
                        )
                        accuracy += (torch.sum(pred == image_masks) / size_pred) * 100
                        diceloss += dice_loss(pred, image_masks)

                train_losses.append(running_loss / len(train_loader))
                test_losses.append(test_loss / len(validation_loader))

                print(
                    f"Epoch {epoch+1}/{epochs}.. "
                    f"Train loss: {running_loss/print_every:.3f}.. "
                    f"Validation loss: {test_loss/len(validation_loader):.3f}.. "
                    f"Valisation accuracy: {accuracy/len(validation_loader):.3f}.."
                    f"Validation Dice Loss: {diceloss/len(validation_loader):.3f}"
                )
                a = [image_masks[0], pred[0].double()]
                a = torch.stack(a)
                write.add_scalar("training loss", running_loss / print_every, steps)
                write.close()
                write.add_scalar(
                    "validation loss", test_loss / len(validation_loader), steps
                )
                write.close()
                img_grid = torchvision.utils.make_grid(a)
                write.add_image("Image mask and Output mask", img_grid, steps)
                write.close()
                out = [
                    inputs[0],
                    inputs[0] * image_masks[0],
                    inputs[0] * pred[0].double(),
                ]
                out = torch.stack(out)
                img_grid = torchvision.utils.make_grid(out)
                write.add_image(
                    "Initial Image and Image Bayesian and Output UNet", img_grid, steps
                )
                write.close()
                running_loss = 0
                model.train()
        torch.save(model.state_dict(), path_to_save_weights + "/model_Unet_PNG.pth")
    return


if __name__ == "__main__":

    if len(sys.argv) < 4:

        sys.stderr.write(
            "Usage: output_path_weights input_path_images input_path_masks"
        )

        sys.exit(0)

    if not os.path.isdir(sys.argv[1]):

        sys.stderr.write("{} is not a folder\n".format(sys.argv[1]))

        sys.exit(0)

    output_path_weights = sys.argv[1]

    input_path_images = sys.argv[2]

    input_path_masks = sys.argv[3]

    print("Creating dataloader..")
    input_path_images = sorted(
        glob.glob(input_path_images + "/*.png"), key=stringSplitByNumbers
    )

    intial_ds = LargeDataset(input_path_images, mask=False)

    input_path_masks = sorted(
        glob.glob(input_path_masks + "/*.png"), key=stringSplitByNumbers
    )
    gt_ds = LargeDataset(input_path_masks, mask=True)

    dataset = bsds_dataset(intial_ds, gt_ds)

    train_loader, validation_loader = split_train_valid(intial_ds, gt_ds)

    print("Training network..")

    training(
        train_loader,
        validation_loader,
        output_path_weights,
        epochs=50,
        steps=0,
        print_every=100,
    )
