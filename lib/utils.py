import glob
import math
import re
from itertools import chain

import cv2
import numpy as np
import scipy.stats as stats
import torch
from scipy.optimize import minimize
from scipy.special import expit
from scipy.stats import uniform
from sklearn.mixture import GaussianMixture
from torch.utils.data import Dataset
from torch.utils.data.sampler import SubsetRandomSampler


def stringSplitByNumbers(x):
    r = re.compile("(\d+)")
    l = r.split(x)
    return [int(y) if y.isdigit() else y for y in l]


class bsds_dataset(Dataset):
    def __init__(self, ds_main, ds_energy, transform=None):
        self.dataset1 = ds_main
        self.dataset2 = ds_energy
        self.transform = transform

    def __getitem__(self, index):
        x1 = self.dataset1[index]
        x2 = self.dataset2[index]
        if self.transform:
            x1 = self.transform(x1)
            x2 = self.transform(x2)

        return x1, x2

    def __len__(self):
        return len(self.dataset1)


def split_train_valid(intial_ds, gt_ds):
    dataset = bsds_dataset(intial_ds, gt_ds)
    validation_split = 0.2
    shuffle_dataset = False
    random_seed = 42
    dataset_size = len(dataset)
    indices = list(range(dataset_size))
    split = int(np.floor(validation_split * dataset_size))
    if shuffle_dataset:
        np.random.seed(random_seed)
        np.random.shuffle(indices)
    train_indices, val_indices = indices[split:], indices[:split]
    train_sampler = SubsetRandomSampler(train_indices)
    valid_sampler = SubsetRandomSampler(val_indices)
    train_loader = torch.utils.data.DataLoader(
        dataset, batch_size=1, sampler=train_sampler
    )
    validation_loader = torch.utils.data.DataLoader(
        dataset, batch_size=1, sampler=valid_sampler
    )
    return train_loader, validation_loader


def dice_loss(pred, target):
    """This definition generalize to real valued pred and target vector.
This should be differentiable.
    pred: tensor with first dimension as batch
    target: tensor with first dimension as batch
    """

    smooth = 1.0

    # have to use contiguous since they may from a torch.view op
    iflat = pred.contiguous().view(-1)
    tflat = target.contiguous().view(-1)
    intersection = (iflat * tflat).sum()

    A_sum = torch.sum(tflat * iflat)
    B_sum = torch.sum(tflat * tflat)

    return 1 - ((2.0 * intersection + smooth) / (A_sum + B_sum + smooth))


## region of interest’s modeling : two lines intersecting and two parabolas


def y1(x, n, m, eps1, eps3, eps5, eps8):
    a = (-m + eps8 + eps3) / (n / 2 - eps5 - eps1)
    b = eps3 - (n / 2 - eps1) * a
    y = a * x + b
    return y


def y2(x, n, m, eps2, eps4, eps6, eps7):
    a = (m - eps7 - eps4) / (n / 2 - eps6 - eps2)
    b = eps4 - (n / 2 + eps2) * a
    y = a * x + b
    return y


def y3(x, n, m, eps1, eps2, eps3, eps4, eps10):
    a = (eps2 * (eps3 - eps10) + eps1 * (eps4 - eps10)) / (eps1 * eps2 * (eps1 + eps2))
    b = (
        (eps10 - eps3) * eps2 * n
        - eps1 * (eps4 - eps10) * (n - eps1)
        + (eps10 - eps3) * (eps2 ** 2)
    ) / (eps1 * eps2 * (eps1 + eps2))
    c = eps2 * (eps3 * (n ** 2) - eps10 * ((n ** 2) - 4 * (eps1 ** 2)))
    c += 2 * (eps2 ** 2) * (eps3 * n - eps10 * (n - 2 * eps1))
    c += eps1 * (eps4 - eps10) * n * (n - 2 * eps2)
    c /= 4 * (eps1 * eps2 * (eps1 + eps2))
    y = a * (x ** 2) + b * x + c
    return y


def y4(x, n, m, eps5, eps6, eps7, eps8, eps9):
    d = (n - 2 * eps5) * (n - 2 * eps6) * (-eps5 - eps6 + n)
    a = (
        -2
        * (
            eps7 * n
            + eps8 * n
            - 2 * eps9 * n
            - 2 * eps5 * eps7
            - 2 * eps6 * eps8
            + 2 * eps5 * eps9
            + 2 * eps6 * eps9
        )
        / d
    )
    b = (
        -eps7 * (n ** 2)
        - 3 * eps8 * (n ** 2)
        + 4 * eps9 * (n ** 2)
        + 8 * eps6 * eps8 * n
        - 8 * eps6 * eps9 * n
        + 4 * (eps5 ** 2) * eps7
    )
    b += -4 * (eps6 ** 2) * (eps8) - 4 * (eps5 ** 2) * eps9 + 4 * (eps6 ** 2) * eps9
    b = -b / d
    c = (
        3 * eps5 * m * (n ** 2)
        + 3 * eps6 * m * (n ** 2)
        - 2 * (eps5 ** 2) * m * n
        - 2 * (eps6 ** 2) * m * n
        - 8 * (eps5 * eps6 * m * n)
    )
    c += (
        4 * eps5 * (eps6 ** 2) * m
        + 4 * eps6 * (eps5 ** 2) * m
        + eps8 * (n ** 3)
        + eps5 * eps7 * (n ** 2)
        - 3 * eps6 * eps8 * (n ** 2)
    )
    c += (
        -4 * eps5 * eps9 * (n ** 2)
        - 2 * (eps5 ** 2) * (eps7 * n)
        + 2 * (eps6 ** 2) * eps8 * n
    )
    c += (
        4 * (eps5 ** 2) * eps9 * n
        + 8 * eps5 * eps6 * eps9 * n
        - 4 * eps5 * (eps6 ** 2) * eps9
        - 4 * (eps5 ** 2) * (eps6 * eps9)
        - m * (n ** 3)
    )
    c = -c / d
    y = a * (x ** 2) + b * x + c
    return y


def get_roi_params(path):
    X = []
    for img in path:
        src = cv2.imread(img)
        image_gray = cv2.cvtColor(src, cv2.COLOR_BGR2GRAY)
        n = src.shape[1]
        m = src.shape[0]
        srcc = image_gray[
            int(m / 2) - 100 : int(m / 2) + 100, int(n / 2) - 100 : int(n / 2) + 100
        ]
        srcc = np.uint8(srcc)
        X.append(srcc.flatten())
    X_norm = list(chain.from_iterable(X))
    X_norm = np.array(X_norm).reshape(-1, 1)
    GMM = GaussianMixture(n_components=2).fit(X_norm)  # Instantiate and fit the model
    print("Converged:", GMM.converged_)  # Check if the model has converged
    means = GMM.means_
    covariances = GMM.covariances_
    pis = GMM.weights_
    return means, covariances, pis


def get_sigm_mean(
    n, m, lambda_, eps1, eps2, eps3, eps4, eps5, eps6, eps7, eps8, eps9, eps10
):
    x = np.arange(n)
    sigm = (1 / 4) * np.array(
        [
            expit((y - y1(x, n, m, eps1, eps3, eps5, eps8)) / lambda_)
            + expit((y - y2(x, n, m, eps2, eps4, eps6, eps7)) / lambda_)
            + expit((y - y3(x, n, m, eps1, eps2, eps3, eps4, eps10)) / lambda_)
            + expit((y4(x, n, m, eps5, eps6, eps7, eps8, eps9) - y) / lambda_)
            for y in range(m)
        ]
    )
    return sigm


def get_sigm_prod(
    n, m, lambda_, eps1, eps2, eps3, eps4, eps5, eps6, eps7, eps8, eps9, eps10
):
    x = np.arange(n)

    sigm = np.array(
        [
            expit((y - y1(x, n, m, eps1, eps3, eps5, eps8)) / lambda_)
            * expit((y - y2(x, n, m, eps2, eps4, eps6, eps7)) / lambda_)
            * expit((y - y3(x, n, m, eps1, eps2, eps3, eps4, eps10)) / lambda_)
            * expit((y4(x, n, m, eps5, eps6, eps7, eps8, eps9) - y) / lambda_)
            for y in range(m)
        ]
    )
    return sigm


def compute_un(theta, src, means, covariances, pis, mu, std, lambda_):
    n = src.shape[1]
    m = src.shape[0]
    image_gray = cv2.cvtColor(src, cv2.COLOR_BGR2GRAY)
    fn = np.array(
        [
            (
                pis[0]
                * stats.norm.pdf(image_gray[y, :], means[0], math.sqrt(covariances[0]))
                + pis[1]
                * stats.norm.pdf(image_gray[y, :], means[1], math.sqrt(covariances[1]))
            )
            for y in range(m)
        ]
    )
    pdf_uni = uniform(mu, std)
    bn = np.array(
        [pdf_uni.pdf(image_gray[y, :]) + (image_gray[y, :] <= 1) for y in range(m)]
    )
    rn = fn / (bn + fn)
    eps1, eps2, eps3, eps4, eps5, eps6, eps7, eps8, eps9, eps10 = theta + np.array(
        [n / 8, n / 8, m / 20, m / 20, n / 8, n / 8, m / 10, m / 10, m / 15, m / 10]
    )
    sigm = get_sigm_mean(
        n, m, lambda_, eps1, eps2, eps3, eps4, eps5, eps6, eps7, eps8, eps9, eps10
    )
    un = rn * sigm / (rn * sigm + (1 - rn) * (1 - sigm) + 1e-6)
    return un, rn


def compute_likeli(theta, src, means, covariances, pis, mu, std, lambda_, un):
    n = src.shape[1]
    m = src.shape[0]
    image_gray = cv2.cvtColor(src, cv2.COLOR_BGR2GRAY)
    eps1, eps2, eps3, eps4, eps5, eps6, eps7, eps8, eps9, eps10 = theta + np.array(
        [n / 8, n / 8, m / 20, m / 20, n / 8, n / 8, m / 10, m / 10, m / 15, m / 10]
    )
    sigm = get_sigm_prod(
        n, m, lambda_, eps1, eps2, eps3, eps4, eps5, eps6, eps7, eps8, eps9, eps10
    )
    mean = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0]).reshape(-1, 1)
    cov = (
        np.diag(
            [
                n / 4,
                n / 4,
                (m / 10) * 10,
                (m / 10) * 10,
                n / 4,
                n / 4,
                m / 5,
                m / 5,
                m / 7,
                m / 5,
            ]
        )
        * 10
    )
    inv_sigma = np.linalg.inv(cov)
    res1 = (theta - mean).T.dot(inv_sigma).dot(theta - mean) / 2
    tmp = (un) * np.log(sigm + 1e-6) + np.log(1 - sigm + 1e-6) * (1 - un)
    likeli = np.mean(-tmp)
    return likeli


def fit_EM(theta, src, means, covariances, pis, mu, std, k, lambda_, e=0.05):

    theta0 = theta.copy()
    srci = src.copy()
    diff_loss = e + 1
    i = 0
    un0, rn0 = compute_un(theta0, src, means, covariances, pis, mu, std, lambda_)
    likeli = [
        compute_likeli(theta0, src, means, covariances, pis, mu, std, lambda_, un0) + 10
    ]

    while np.abs(diff_loss) > e:
        un, rn = compute_un(theta, src, means, covariances, pis, mu, std, lambda_)
        res = minimize(
            compute_likeli,
            theta,
            args=(src, means, covariances, pis, mu, std, lambda_, un),
            method="L-BFGS-B",
            options={"disp": True},
        )
        theta = res.x
        likeli += [
            compute_likeli(theta, src, means, covariances, pis, mu, std, lambda_, un)
        ]
        diff_loss = likeli[i] - likeli[i + 1]
        i += 1

    print("Fit Gaussian converged after %i iterations" % i)
    return res


def plot_result(path_res, src, res, theta0, k):
    n = src.shape[1]
    m = src.shape[0]
    srcc = np.zeros((m, n, 3))
    eps1, eps2, eps3, eps4, eps5, eps6, eps7, eps8, eps9, eps10 = res.x + np.array(
        [n / 8, n / 8, m / 20, m / 20, n / 8, n / 8, m / 10, m / 10, m / 15, m / 10]
    )
    for x in range(n):
        for y in range(m):
            if (
                y1(x, n, m, eps1, eps3, eps5, eps8) < y
                and y2(x, n, m, eps2, eps4, eps6, eps7) < y
                and y3(x, n, m, eps1, eps2, eps3, eps4, eps10) < y
                and y4(x, n, m, eps5, eps6, eps7, eps8, eps9) > y
            ):
                srcc[y, x, :] = [255, 255, 255]
    srcc = np.uint8(srcc)
    cv2.imwrite(path_res + "/image_%d.png" % (k), srcc)
    return


def optimize_EM(path_imgs, path_res):
    path = sorted(glob.glob(path_imgs + "/*.png"), key=stringSplitByNumbers)

    means, covariances, pis = get_roi_params(path)

    for k, img in enumerate(path):

        src = cv2.imread(img)
        n = src.shape[1]
        m = src.shape[0]
        theta0 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        mu, std = 100 / 2, ((300 ** 2) / 12) ** (1 / 2)
        res = fit_EM(
            theta0, src, means, covariances, pis, mu, std, k, lambda_=(m / 100)
        )
        plot_result(path_res, src, res, theta0, k)

    print("optimization completed")
