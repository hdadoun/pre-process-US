import os
import sys

from lib.utils import *

if __name__ == "__main__":

    if len(sys.argv) < 3:

        sys.stderr.write("Usage: output_path_masks input_path_images")

        sys.exit(0)

    if not os.path.isdir(sys.argv[1]):

        sys.stderr.write("{} is not a folder\n".format(sys.argv[1]))

        sys.exit(0)

    output_path_masks = sys.argv[1]

    input_path_images = sys.argv[2]

    optimize_EM(input_path_images, output_path_masks)
