import glob
import os
import sys

import cv2
import numpy as np
import torch
from skimage.restoration import denoise_nl_means, estimate_sigma
from torch.autograd import Variable

from lib.model import UNet
from lib.utils_test import (LargeDataset, delete_color, detect_annotation,
                            detect_color, stringSplitByNumbers)


def inference_Unet(dataloader, input_path, output_path_processed):
    model = UNet(3, 1)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model.to(device)
    model.load_state_dict(torch.load("weights/model_Unet_9.pth", map_location=device))
    patch_kw = dict(
        patch_size=5,  # 5x5 patches
        patch_distance=6,  # 13x13 search area
        multichannel=True,
    )
    model.eval()
    with torch.no_grad():
        for i, inputs in enumerate(dataloader):
            inputs = Variable(inputs).transpose_(1, 3).to(device)
            dec = model(inputs)
            result = torch.sigmoid(dec) > 0.5
            mask = inputs[0].transpose_(1, 2).type(torch.double) * result[0].transpose_(
                1, 2
            ).type(torch.double)
            out = mask.detach().cpu().numpy().transpose(1, 2, 0)
            output = np.clip(out * 255, 0, 255)  # proper [0..255] range
            output = output.astype(np.uint8)  # safe conversion
            if detect_color(output):
                pass
            else:
                image_grey = delete_color(output)
                mask_inpaint = detect_annotation(output)
                dst = cv2.inpaint(image_grey, mask_inpaint, 3, cv2.INPAINT_TELEA)
                # median = cv2.medianBlur(dst, 3) # If you want to use a median filter
                sigma_est = np.mean(estimate_sigma(dst, multichannel=True))
                median = denoise_nl_means(
                    dst, h=0.8 * sigma_est, fast_mode=True, **patch_kw
                )
                cv2.imwrite(
                    output_path_processed + "/%s" % os.path.basename(input_path[i]),
                    median,
                )

    return


if __name__ == "__main__":

    if len(sys.argv) < 3:

        sys.stderr.write("Usage: output_path_processed input_path_images")

        sys.exit(0)

    if not os.path.isdir(sys.argv[1]):

        sys.stderr.write("{} is not a folder\n".format(sys.argv[1]))

        sys.exit(0)

    output_path_processed = sys.argv[1]

    input_path_images = sys.argv[2]

    input_path = sorted(
        glob.glob(input_path_images + "/*.jpg"), key=stringSplitByNumbers
    )
    print(input_path)
    test_ds = LargeDataset(input_path, mask=False)

    dataloader = torch.utils.data.DataLoader(test_ds, batch_size=1, shuffle=False)
    print("Number of samples: ", len(test_ds))

    inference_Unet(dataloader, input_path, output_path_processed)
