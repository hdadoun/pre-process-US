## Pre-processing of US JPEG images :

This is a pytorch implementation for 
1-Automated delineation of the border of the fan in echocardiography (generating masks of the fan using EM algorithm in mask_generation.py, training Unet on those masks in train.py) 
2-Filtering Doppler Images (in test.py)
3-Inpainting images with annotations for unbiased training (in test.py) 

These are the steps to follow if you simply want to process your images : 

Open a terminal : pip install virtualenvwrapper

Create virtual env : mkvirtualenv -p python3 us-pre-processing

Activate virtual env :  on linux : workon us-pre-processing 

Install required packages for train: pip install -r  requirements.txt

Install required packages for inference : pip install -r  requirements_test.txt

Run main.py : python test.py path_where_to_output_results path_of_input_images 



 

